using System;
using System.Security.Cryptography;

namespace SymmetricEncryption
{
	public class AesGcmEncryption : IEncryptionAlgorithm
	{
		private readonly AesGcm _aesGcm;
		private string _key;
		public byte[] Key { get; }
		public string IV { get; }
		public byte[] Nonce { get; }

		public AesGcmEncryption()
		{
			Key = new byte[16]; // 128-bit key
			Nonce = new byte[12]; // 96-bit nonce

			var rng = new RNGCryptoServiceProvider();
			rng.GetBytes(Key);
			rng.GetBytes(Nonce);

			_aesGcm = new AesGcm(Key);
		}

		public string KeyBase64 => Convert.ToBase64String(Key);
		public string NonceBase64 => Convert.ToBase64String(Nonce);

		string IEncryptionAlgorithm.Key => _key;

		public string Encrypt(string message)
		{
			byte[] plaintext = System.Text.Encoding.UTF8.GetBytes(message);
			byte[] ciphertext = new byte[plaintext.Length];
			byte[] tag = new byte[16];

			_aesGcm.Encrypt(Nonce, plaintext, ciphertext, tag);

			byte[] combined = new byte[ciphertext.Length + tag.Length];
			ciphertext.CopyTo(combined, 0);
			tag.CopyTo(combined, ciphertext.Length);

			return Convert.ToBase64String(combined);
		}

		public string Decrypt(string encryptedMessage)
		{
			byte[] combined = Convert.FromBase64String(encryptedMessage);
			byte[] ciphertext = new byte[combined.Length - 16];
			byte[] tag = new byte[16];

			Array.Copy(combined, 0, ciphertext, 0, ciphertext.Length);
			Array.Copy(combined, ciphertext.Length, tag, 0, tag.Length);

			byte[] plaintext = new byte[ciphertext.Length];

			_aesGcm.Decrypt(Nonce, ciphertext, tag, plaintext);

			return System.Text.Encoding.UTF8.GetString(plaintext);
		}
	}
}