using System.Security.Cryptography;

namespace SymmetricEncryption;

public class DesEncryption : EncryptionAlgorithmBase {
	public DesEncryption() {
		Algorithm = DES.Create();
	}
}