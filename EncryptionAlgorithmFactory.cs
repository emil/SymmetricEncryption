namespace SymmetricEncryption;

public static class EncryptionAlgorithmFactory {
	public static IEncryptionAlgorithm Create(Algorithm algorithmType) {
		return algorithmType switch {
			Algorithm.DES => new DesEncryption(),
			Algorithm.TripleDES => new TripleDesEncryption(),
			Algorithm.AES => new AesEncryption(),
			Algorithm.AESGCM => new AesGcmEncryption(),
			_ => throw new ArgumentException("Invalid algorithm type", nameof(algorithmType)),
		};
	}
}