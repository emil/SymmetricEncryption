using System;
using System.IO;
using System.Security.Cryptography;

namespace SymmetricEncryption;

public interface IEncryptionAlgorithm {
	string Key { get; }
	string IV { get; }
	string Encrypt(string message);
	string Decrypt(string encryptedMessage);
}

public abstract class EncryptionAlgorithmBase : IEncryptionAlgorithm {
	protected SymmetricAlgorithm Algorithm;
	public string Key => Convert.ToBase64String(Algorithm.Key);
	public string IV => Convert.ToBase64String(Algorithm.IV);

	public virtual string Encrypt(string message) {
		using var encryptor = Algorithm.CreateEncryptor();
		using var memoryStream = new MemoryStream();
		using var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
		using var writer = new StreamWriter(cryptoStream);
		writer.Write(message);
		writer.Flush();
		cryptoStream.FlushFinalBlock();
		return Convert.ToBase64String(memoryStream.ToArray());
	}

	public virtual string Decrypt(string encryptedMessage) {
		using var decryptor = Algorithm.CreateDecryptor();
		var bytes = Convert.FromBase64String(encryptedMessage);
		using var memoryStream = new MemoryStream(bytes);
		using var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
		using var reader = new StreamReader(cryptoStream);
		return reader.ReadToEnd();
	}
}