using System.Security.Cryptography;

namespace SymmetricEncryption; 

public class TripleDesEncryption : EncryptionAlgorithmBase {
	public TripleDesEncryption() {
		Algorithm = TripleDES.Create();
	}
}