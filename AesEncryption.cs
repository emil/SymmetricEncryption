using System.Security.Cryptography;

namespace SymmetricEncryption;

public class AesEncryption : EncryptionAlgorithmBase {
	public AesEncryption() {
		Algorithm = Aes.Create();
	}
}