﻿using System.Diagnostics;
using SymmetricEncryption;

var algorithmChoice = GetUserChoice("Choose encryption algorithm:\n  1. DES\n  2. TripleDES\n  3. AES\n  4. AES GCM", 1, 4);
var algorithm = (Algorithm) (algorithmChoice - 1);
var encryptionAlgorithm = EncryptionAlgorithmFactory.Create(algorithm);

Console.Clear();

var message = GetUserInput("Choose a message to encrypt: ");

PerformEncryption(encryptionAlgorithm, message);

int GetUserChoice(string prompt, int lowerLimit, int upperLimit) {
	int choice;
	do {
		Console.WriteLine(prompt);
		int.TryParse(Console.ReadLine(), out choice);
	} while (choice < lowerLimit || choice > upperLimit);

	return choice;
}

string GetUserInput(string prompt) {
	string input;
	do {
		Console.WriteLine(prompt);
		input = Console.ReadLine();
	} while (string.IsNullOrEmpty(input));

	return input;
}

void PerformEncryption(IEncryptionAlgorithm algorithm, string message) {
	var stopwatch = new Stopwatch();
	stopwatch.Start();
	var encryptedMessage = algorithm.Encrypt(message);
	stopwatch.Stop();

	Console.WriteLine($"\nKey: {algorithm.Key}");
	Console.WriteLine($"IV: {algorithm.IV}");
	Console.WriteLine($"\nEncrypted message: {encryptedMessage}");
	Console.WriteLine($"\nEncryption time: {stopwatch.Elapsed}");

	stopwatch.Restart();
	var decryptedMessage = algorithm.Decrypt(encryptedMessage);
	stopwatch.Stop();

	Console.WriteLine($"\nDecrypted message: {decryptedMessage}");
	Console.WriteLine($"\nDecryption time: {stopwatch.Elapsed}");
}


public enum Algorithm {
	DES,
	TripleDES,
	AES,
	AESGCM,
}